import routes from '../api'
import bodyParser from 'body-parser';
import cors from 'cors';

const allowedOrigins = [ //тут хранится список/массив разрешенных доменов, с которых можно отправлять запрос на данный сервер
    'http://localhost:3000',
    'http://localhost:3001',
    'https://step-inst-parody-fe.herokuapp.com'
]

export default async ({app, config}) => {  //модуль должен возвращать функцию

    app.use(cors(
        {
            origin: function (origin, callback) {
                if (!origin) {
                    return callback(null, true);
                }
                if (allowedOrigins.indexOf(origin) === -1) {
                    const msg = 'The Cors policy for this site does not allow access from specified Origin.';

                    return callback(new Error(msg), false)
                }
                return callback(null, true);
            },
            credentials: true
        }
    ));
    app.use(bodyParser.json()); //bodyParser = middleware
    //bodyParser отлавливает данные POST запроса и преобразует их в json и добавляет их к объекту request

    //Подгружаем API routes
    app.use(`${config.SERVICE_PREFIX}`, routes()) // ставится самым последним
}