import expressLoader from './express';
import {connectToMongoDb as mongoDBLoader} from './mongodb'

export default async ({app, config}) => {
    //должен возвращать функцию которая принимает 1 переметр

    await mongoDBLoader({config})// выполняем написанный лоадер
    console.log('Mongo connected')

    await expressLoader({app, config})// выполняем написанный лоадер
    console.log('Express loaded')


}