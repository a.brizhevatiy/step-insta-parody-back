//корневой модуль для API
import {Router} from 'express';
import users from './routes/users'; // подключили модуль Юзерс
import posts from './routes/posts'
import likes from './routes/likes'
import subscribes from './routes/subscribes'
import comments from './routes/comments'


export default () => {
    const router = Router();

    users(router); // вызвали Юзерс передав в нее функцию
    posts(router)
    likes(router)
    subscribes(router)
    comments(router)

    return router; // данный созданный Роут необходимо перехватить в Лоадерах
    // и перепдать приложению Express
}