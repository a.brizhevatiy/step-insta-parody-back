// в данном файле задача прописать все возможные запросы для Роута Юзерс
import {Router} from 'express';
import {createPostController, getPostsController, getPostController, putPostController} from "./controller";

// Router из пакета Express используется для управления Роутами на сайте
const route = Router();

export default mainRoute => {
    mainRoute.use('/posts', route)
    /**
     * @desc post запрос на роут /posts,
     * http://url/api/posts
     * **/
    // используется функция, вынесенная в posts/controller
    // Запрос на запись в БД
    route.post('/', createPostController)
    /**
     * @desc get запрос на роут /posts, вывод списка постов
     * http://url/api/posts
     * **/
    route.get('/', getPostsController)
    /**
     * @desc get запрос на роут /post, вывод поста
     * http://url/api/posts/id
     * **/
    route.get('/:id', getPostController)
    /**
     * @desc put запрос на роут /posts/id,
     * http://url/api/posts/id
     * **/
    route.put('/:id', putPostController)
}