// бизнес-логика
import PostService from "../../../services/postService";

/*
* @decs  Логика по обработке создания постов
* */

export const createPostController = async (req, res, next) => {
    const {
        postId,
        authorId,
        image,
        title
    } = req.body;// получаем данные из запроса

    const post = await PostService.createPost({ //создание запроса описано в postService
        postId,
        authorId,
        image,
        title
    })

    return res.json({ // выводим данные как ответ на запрос
        success: true,
        data: post
    })
}

export const getPostsController = async (req, res, next) => {
    const posts = await PostService.getPosts();

    return res.json({
        success: true,
        data: posts
    })
}
export const getPostController = async (req, res, next) => {
    const post = await PostService.getPost(req.params.id);

    return res.json({
        success: true,
        data: post
    })
}
export const putPostController = async (req, res, next) => {
    const post = await PostService.putPost(req.params.id, req.body);

    return res.json({
        success: true,
        data: post
    })
}