// в данном файле задача прописать все возможные запросы для Роута Юзерс
import {Router} from 'express';
import {createUserController, getUsersController, getUserController} from "./controller";

// Router из пакета Express используется для управления Роутами на сайте
const route = Router();

export default mainRoute => {
    mainRoute.use('/users', route)
    /**
     * @desc post запрос на роут /users,
     * http://url/api/users
     * **/
    // используется функция, вынесенная в users/controller
    route.post('/', createUserController// Запрос на запись в БД
    )
    /**
     * @desc get запрос на роут /users, вывод списка пользователей
     * http://url/api/users
     * **/
    route.get(
        '/', getUsersController)

    route.get(
        '/:id', getUserController)

    /**
     * @desc post запрос на роут /users,
     * http://url/api/users/id
     * **/
    route.put('/:id',
        (req,
         res,
         next) => {

        })
}