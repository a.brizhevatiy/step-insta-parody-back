// бизнес-логика
import UserService from "../../../services/userService";

/*
* @decs  Логика по обработке создания пользователя
* */

export const createUserController = async (req, res, next) => {
    const {
        id,
        name,
        username,
        email,
        photo,
        password
    } = req.body;// получаем данные из запроса

    const user = await UserService.createUser({ //создание запроса описано в userService
        id,
        name,
        username,
        email,
        photo,
        password
    })

    return res.json({ // выводим данные как ответ на запрос
        success: true,
        data: user
    })
}

export const getUsersController = async (req, res, next) => {
    const users = await UserService.getUsers();
    return res.json({
        success: true,
        data: users
    })
}
export const getUserController = async (req, res, next) => {
    const user = await UserService.getUser(req.params.id);

    return res.json({
        success: true,
        data: user
    })
}