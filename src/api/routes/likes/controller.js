// бизнес-логика
import LikeService from "../../../services/likeService";

/*
* @decs  Логика по обработке создания лайков
* */

export const createLikeController = async (req, res, next) => {
    const {
        postId,
        likedBy
    } = req.body;// получаем данные из запроса

    const likes = await LikeService.createLike({ //создание запроса описано в postService
        postId,
        likedBy
    })

    return res.json({ // выводим данные как ответ на запрос
        success: true,
        data: likes
    })
}

export const getLikesController = async (req, res, next) => {
    const likes = await LikeService.getLikes();
    return res.json({
        success: true,
        data: likes
    })
}

export const getLikeController = async (req, res, next) => {
    const like = await LikeService.getLike(req.params.id);

    return res.json({
        success: true,
        data: like[0].likedBy
    })
}


export const putLikeController = async (req, res, next) => {
    const like = await LikeService.putLike(req.params.id, req.body);

    return res.json({
        success: true,
        data: like
    })
}
