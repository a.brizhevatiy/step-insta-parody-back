// в данном файле задача прописать все возможные запросы для Роута Юзерс
import {Router} from 'express';
import {createLikeController, getLikesController, getLikeController, putLikeController} from "./controller";

// Router из пакета Express используется для управления Роутами на сайте
const route = Router();

export default mainRoute => {
    mainRoute.use('/likes', route)
    /**
     * @desc post запрос на роут /posts,
     * http://url/api/likes
     * **/
    // используется функция, вынесенная в posts/controller
    route.post('/', createLikeController)
    /**
     * @desc get запрос на роут /posts, вывод списка постов
     * http://url/api/likes
     * **/
    route.get('/', getLikesController)
    /**
     * @desc get запрос на роут /post, вывод поста
     * http://url/api/likes/id
     * **/
    route.get('/:id', getLikeController)
    /**
     * @desc put запрос на роут /post, вывод поста
     * http://url/api/likes/id
     * **/
    route.put('/:id', putLikeController)
}