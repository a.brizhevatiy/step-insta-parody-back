// бизнес-логика
import CommentService from "../../../services/commentService";

/*
* @decs  Логика по обработке создания комментов
* */

export const createCommentController = async (req, res, next) => {
    const {
        postId,
        comments
    } = req.body;// получаем данные из запроса

    const allComments = await CommentService.createComment({ //создание запроса описано в commentService
        postId,
        comments
    })

    return res.json({ // выводим данные как ответ на запрос
        success: true,
        data: allComments
    })
}

export const getCommentsController = async (req, res, next) => {
    const comments = await CommentService.getComments();
    return res.json({
        success: true,
        data: comments
    })
}

export const getCommentController = async (req, res, next) => {
    const comment = await CommentService.getComment(req.params.id);
    return res.json({
        success: true,
        data: comment[0].comments
    })
}


export const putCommentController = async (req, res, next) => {
    const comment = await CommentService.putComment(req.params.id, req.body);

    return res.json({
        success: true,
        data: comment
    })
}
