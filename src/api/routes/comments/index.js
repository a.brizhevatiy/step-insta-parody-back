// в данном файле задача прописать все возможные запросы для Роута Юзерс
import {Router} from 'express';
import {createCommentController, getCommentsController, getCommentController, putCommentController} from "./controller";

// Router из пакета Express используется для управления Роутами на сайте
const route = Router();

export default mainRoute => {
    mainRoute.use('/comments', route)
    /**
     * @desc post запрос на роут /posts,
     * http://url/api/comments
     * **/
    // используется функция, вынесенная в comments/controller
    route.post('/', createCommentController)
    /**
     * @desc get запрос на роут /posts, вывод списка постов
     * http://url/api/comments
     * **/
    route.get('/', getCommentsController)
    /**
     * @desc get запрос на роут /post, вывод поста
     * http://url/api/comments/id
     * **/
    route.get('/:id', getCommentController)
    /**
     * @desc put запрос на роут /post, вывод поста
     * http://url/api/comments/id
     * **/
    route.put('/:id', putCommentController)
}