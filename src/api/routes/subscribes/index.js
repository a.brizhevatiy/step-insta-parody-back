// в данном файле задача прописать все возможные запросы для Роута Юзерс
import {Router} from 'express';
import {
    createSubscribeController,
    getSubscribesController,
    getSubscribeController,
    putSubscribeController
} from "./controller";
import cors from "cors";

// Router из пакета Express используется для управления Роутами на сайте
const route = Router();

export default mainRoute => {
    mainRoute.use('/subscribes', route)
    /**
     * @desc post запрос на роут /subscribes,
     * http://url/api/subscribes
     * **/
    // используется функция, вынесенная в subscribes/controller
    route.post('/', createSubscribeController)
    /**
     * @desc get запрос на роут /subscribes, вывод списка постов
     * http://url/api/subscribes
     * **/
    route.get('/', getSubscribesController)
    /**
     * @desc get запрос на роут /subscribes, вывод поста
     * http://url/api/subscribes/id
     * **/
    route.get('/:id', getSubscribeController)
    /**
     * @desc put запрос на роут /subscribes, вывод поста
     * http://url/api/subscribes/id
     * **/
    route.put('/:id', putSubscribeController)
}