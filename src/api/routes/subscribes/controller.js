// бизнес-логика
import subscribesService from "../../../services/subscribeService";

/*
* @decs  Логика по обработке создания лайков
* */

export const createSubscribeController = async (req, res, next) => {
    const {
        userId,
        subscribed
    } = req.body;// получаем данные из запроса

    const subscribe = await subscribesService.createSubscribe({ //создание запроса описано в postService
        userId,
        subscribed
    })

    return res.json({ // выводим данные как ответ на запрос
        success: true,
        data: subscribe
    })
}

export const getSubscribesController = async (req, res, next) => {
    const subscribes = await subscribesService.getSubscribes();
    return res.json({
        success: true,
        data: subscribes
    })
}

export const getSubscribeController = async (req, res, next) => {
    const subscribe = await subscribesService.getSubscribe(req.params.id);
    return res.json({
        success: true,
        data: subscribe[0].subscribed
    })
}

export const putSubscribeController = async (req, res, next) => {
    const subscribe = await subscribesService.putSubscribe(req.params.id, req.body);

    return res.json({
        success: true,
        data: subscribe
    })
}
