import dotenv from 'dotenv';

const environment = dotenv.config();

if (!environment) {
    throw new Error('Config file was not found');
}

export default { // экспорт объекта, который представляет из мебя настройки данного приложения
    PORT: process.env.PORT || 3005, // порт, на котором работает приложение

    //префикс для используемого АПИ
    // PREFIX: process.env.PREFIX || '/api',
    /**
     * @desc Префікс API
     **/
    SERVICE_PREFIX: '/api',//process.env.SERVICE_PREFIX || '/api',
    VERSION: process.env.VERSION || '1.0.0',

    /**
     *
     **/
    MAILGUN_DOMAIN: process.env.MAILGUN_DOMAIN || null,

    /**
     *
     **/
    MAILGUN_API_KEY: process.env.MAILGUN_API_KEY || null,
//подключение к БД
    MONGODB_URI: process.env.MONGODB_URI || ''
}


//qwe123ASDZXC
//mongodb+srv://my-dbuser:qwe123ASDZXC@cluster0.pybua.mongodb.net/step-parody-app?retryWrites=true&w=majority