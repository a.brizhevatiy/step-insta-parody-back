import mongoose from "mongoose";

const Post = new mongoose.Schema({
    postId: Number,
    authorId: Number,
    image: String,
    title: String
})

export default mongoose.model('Post', Post);