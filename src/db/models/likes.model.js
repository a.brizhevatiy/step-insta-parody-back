import mongoose from "mongoose";

const Likes = new mongoose.Schema({
    postId: Number,
    likedBy: Array
})

export default mongoose.model('Likes', Likes);