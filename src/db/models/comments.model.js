import mongoose from "mongoose";

const Comments = new mongoose.Schema({
    postId: Number,
    comments: Array
})

export default mongoose.model('Comments', Comments);