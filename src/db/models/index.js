import UserModel from './user.model.js';
import PostModel from './post.model.js';
import LikesModel from './likes.model.js';
import CommentsModel from './comments.model.js';
import SubscribesModel from './subscribes.model.js';

export {
    UserModel,
    PostModel,
    LikesModel,
    CommentsModel,
    SubscribesModel
}