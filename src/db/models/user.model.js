import mongoose from "mongoose";

const User = new mongoose.Schema({
    id: Number,
    name: String,
    username: String,
    email: String,
    photo: String,
    password: String
})

export default mongoose.model('User', User);