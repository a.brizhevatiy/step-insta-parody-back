import mongoose from "mongoose";

const Subscribes = new mongoose.Schema({
    userId: Number,
    subscribed: Array
})

export default mongoose.model('Subscribes', Subscribes);