import {CommentsModel} from "../db/models";

export default class commentsService {
    //логика создания лайка
    static async createComment(commentODI) {
        let comment = null;
        const {
            postId,
            comments
        } = commentODI;
        const test = await CommentsModel.find({postId: postId});
        if (!test.length) {
            comment = await CommentsModel.create({
                postId,
                comments
            })
        } else {
            comment = "Already exists."
        }
        return comment;
    }

    static async getComments() {
        const comments = await CommentsModel.find((err, data) => {
            if (err) return err;
            return data;
        });

        let newData = {};
        comments.forEach(i=>{
            newData[i.postId] = i.comments;
        })
        return newData;
        // return comments;
    }

    static async getComment(postId) {
        const comment = await CommentsModel.find({postId: postId});
        return comment;
    }

    static async putComment(postId, newData) {
        const comment = await CommentsModel.updateOne({postId: postId}, newData);
        // console.log(comment.data.nModified)
        return comment;
    }
}
