import {LikesModel} from "../db/models";

export default class likesService {
    //логика создания лайка
    static async createLike(likeODI) {
        let like = null;
        const {
            postId,
            likedBy
        } = likeODI;
        const test = await LikesModel.find({postId: postId});
        if (!test.length) {
            like = await LikesModel.create({
                postId,
                likedBy
            })
        } else {
            like = "Already exists."
        }
        return like;
    }

    static async getLikes() {
        const likes = await LikesModel.find((err, data) => {
            if (err) return err;
            return data;
        });
        let newData = {};
        likes.forEach(i=>{
            newData[i.postId] = i.likedBy;
        })
        return newData;

        // return likes;
    }

    static async getLike(postId) {
        const like = await LikesModel.find({postId: postId});
        return like;
    }

    static async putLike(postId, newData) {
        const like = await LikesModel.updateOne({postId: postId}, newData);
        return like;
    }
}
