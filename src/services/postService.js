import {CommentsModel, PostModel} from "../db/models";

export default class postService {
    //логика создания пользователя
    static async createPost(postODI) {
        let post = null;
        const {
            postId,
            authorId,
            image,
            title
        } = postODI;
        const test = await PostModel.find({postId: postId});
        if (!test.length) {
            post = await PostModel.create({
                postId,
                authorId,
                image,
                title
            })
        } else {
            post = "Already exists."
        }

        return post;
    }

    static async getPosts() {
        const posts = await PostModel.find((err, data) => {
            return data;
        });
        return posts;
    }

    static async getPost(postId) {
        const post = await PostModel.find({postId: postId});
        return post;
    }

    static async putPost(postId, newData) {
        const post = await PostModel.updateOne({postId: postId}, newData);
        return post;
    }

}

