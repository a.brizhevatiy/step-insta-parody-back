import {UserModel} from "../db/models";

export default class userService {
    //логика создания пользователя
    static async createUser(userODI) {
        let user = null;
        const {
            id,
            name,
            username,
            email,
            photo,
            password
        } = userODI;
        const test = await UserModel.find({id: id});
        if (!test.length) {
            user = await UserModel.create({
                id,
                name,
                username,
                email,
                photo,
                password
            })
        } else {
            user = "Already exists."
        }

        return user;
    }

    static async getUsers() {
        const users = await UserModel.find((err, data) => {
            return data;
        });
        return users;
    }

    static async getUser(userId) {
        const user = await UserModel.find({id: userId});
        return user;
    }
}