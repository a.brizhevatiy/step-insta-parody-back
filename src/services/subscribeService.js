import {CommentsModel, SubscribesModel} from "../db/models";

export default class subscribesService {
    //логика создания подписки
    static async createSubscribe(subscribeODI) {
        let subscribe = null;
        const {
            userId,
            subscribed
        } = subscribeODI;
        const test = await SubscribesModel.find({postId: postId});
        if (!test.length) {
            subscribe = await SubscribesModel.create({
                userId,
                subscribed
            })
        } else {
            subscribe = "Already exists."
        }
        return subscribe;
    }

    static async getSubscribes() {
        const subscribes = await SubscribesModel.find((err, data) => {
            if (err) return err;
            return data;
        });
        let newData = {};
        subscribes.forEach(i=>{
            newData[i.userId] = i.subscribed;
        })
        return newData;
        // return subscribes;
    }

    static async getSubscribe(userId) {
        const subscribe = await SubscribesModel.find({userId: userId});
        return subscribe;
    }

    static async putSubscribe(userId, newData) {
        const subscribe = await SubscribesModel.updateOne({userId: userId}, newData);
        return subscribe;
    }
}